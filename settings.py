from dataclasses import dataclass
from typing import Optional

from dotenv import dotenv_values

config: dict[str, Optional[str]] = dotenv_values("conf.txt")  # загружает общие переменные
server_port = config.get("server_port") or 10_000


@dataclass
class Settings:
    """ Настройки клиента """

    WINDOW_TITLE = "Strike, loot, repeat"

    WIDTH_WINDOW = 1440
    HEIGHT_WINDOW = 810
    HALF_WIDTH_WINDOW = WIDTH_WINDOW // 2
    HALF_HEIGHT_WINDOW = HEIGHT_WINDOW // 2

    FPS = 60

    COLORS = {
        0: (200, 200, 0),
        1: (200, 0, 200),
        2: (0, 100, 200),
        3: (100, 60, 200),
        4: (100, 60, 100),
        5: (255, 255, 0),
    }

    SERVER_IP = config["server_ip"]
    SERVER_PORT = int(server_port)

    IMAGE_MAP = {
        0: "images/player-blue.png",
        1: "images/player-cyan.png",
        2: "images/player-gray.png",
        3: "images/player-green.png",
        4: "images/player-orange.png",
        5: "images/player-red.png",
        6: "images/player-viol.png",
        7: "images/player-yellow.png",
    }
