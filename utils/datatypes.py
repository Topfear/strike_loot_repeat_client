from typing import NamedTuple


class Vector(NamedTuple):
    """ Класс описывающий радиус-вектор в двумерном пространстве """
    x_pos: int
    y_pos: int


class Point(NamedTuple):
    """ Класс описывающий точку в двумерном пространстве """
    x_pos: int
    y_pos: int


class PlayerMessage(NamedTuple):
    """ Сообщение игрока серверу """
    x_speed: int
    y_speed: int
    angle: int


class ServerInitData(NamedTuple):
    """ Начальные данные с сервера """
    player_id: int
    radius: int
    room_width: int
    room_hight: int


class ServerData(NamedTuple):
    """ Данные с сервера """
    x_pos: int
    y_pos: int
    objects: list[dict]
