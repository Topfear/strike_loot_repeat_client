import math
import pygame
from pygame.surface import Surface

from settings import Settings
from utils.datatypes import PlayerMessage, Point, ServerInitData, Vector
from game_objects.character import Character


class Player(Character):
    """ Игрок """

    def __init__(self, screen: Surface, init_data: ServerInitData) -> None:
        """ Инициализация игрока """
        super().__init__()
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.player_id = init_data.player_id
        self.angle = 0
        self.radius = init_data.radius
        self.diameter = self.radius * 2

        # загрузка картинки
        self.image = pygame.image.load(Settings.IMAGE_MAP[self.player_id])
        self.image = pygame.transform.scale(self.image, (self.diameter, self.diameter))
        self.rect = self.image.get_rect()

        # игрок по центру экрана
        self.rect.centerx = int(self.screen_rect.centerx)
        self.rect.centery = int(self.screen_rect.centery)

        # вектор передвижения
        self.vector = Vector(x_pos=0, y_pos=0)

        # флаги перемещения
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

        # информация об игроке на прошлом фрейме
        self.prev_info = PlayerMessage(
            x_speed=0,
            y_speed=0,
            angle=0,
        )

    def update(self) -> None:   # type: ignore[override]
        """ Вычисление направления игрока """
        speed = 1
        shift_x = 0
        shift_y = 0

        # обновляет позицию игрока
        if self.moving_right:
            shift_x += speed
        if self.moving_left:
            shift_x -= speed
        if self.moving_up:
            shift_y -= speed
        if self.moving_down:
            shift_y += speed

        self.vector = Vector(x_pos=shift_x, y_pos=shift_y)

    def get_info(self) -> PlayerMessage | None:
        """
        Получить новую информацию от игрока,
        если состояние не изменилось вернётся None
        """
        info = PlayerMessage(
            x_speed=self.vector.x_pos,
            y_speed=self.vector.y_pos,
            angle=self.angle,
        )
        info_changed = info != self.prev_info
        self.prev_info = info
        if info_changed:
            return info
        return None

    def rotate(self, point: Point) -> None:
        """ Повернуть игрока в сторону точки """
        dx = point.x_pos - self.rect.centerx
        dy = point.y_pos - self.rect.centery
        rads = math.atan2(-dy, dx)
        rads %= 2 * math.pi
        degs = math.degrees(rads) - 90
        self.angle = int(degs)
