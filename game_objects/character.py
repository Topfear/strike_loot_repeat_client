import pygame
from pygame.sprite import Sprite


class Character(Sprite):
    """ Общий класс для игрока, других игроков и врагов """

    rect: pygame.rect.Rect
    image: pygame.surface.Surface
    angle: int
    screen: pygame.surface.Surface
    radius: int

    @property
    def x(self) -> int:
        """ Позиция персонажа по оси x """
        return self.rect.centerx

    @property
    def y(self) -> int:
        """ Позиция персонажа по оси y """
        return self.rect.centery

    def draw_shadow(self, offset: int = 5) -> None:
        """ Рисует тень вокруг персонажа с переданным отступом """
        surface = pygame.Surface((110, 110), pygame.SRCALPHA)
        shadow_radius = self.radius + offset
        shadow_color = (0, 0, 0, 75)
        pygame.draw.circle(surface, shadow_color, (shadow_radius, shadow_radius), shadow_radius)
        self.screen.blit(surface, (self.rect.left - offset, self.rect.top - offset))

    def blitme(self) -> None:
        """ Отобразить персонажа с проворотом """
        rotated_img = pygame.transform.rotozoom(self.image, self.angle, 1)
        rotated_rect = rotated_img.get_rect(center=self.rect.center)
        self.draw_shadow(offset=3)
        self.screen.blit(rotated_img, rotated_rect)
