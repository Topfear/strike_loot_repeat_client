from pygame.sprite import Group

from game_objects.enemy import Enemy
from game_objects.player import Player


class AllGroup(Group):
    """ Общая группа для всех объектов на экране """

    def sprites(self) -> list[Player | Enemy]:      # type: ignore[override]
        """ Все спрайты в группе """
        return super().sprites()                    # type: ignore[return-value]
