import pygame
from pygame.sprite import Sprite
from pygame.surface import Surface

from settings import Settings
from utils.datatypes import ServerData, ServerInitData


class Grid(Sprite):
    """ Игровая сетка """

    def __init__(self, screen: Surface, init_data: ServerInitData):
        """ Создать сетку """
        self.screen = screen
        self.width = init_data.room_width
        self.height = init_data.room_hight

        self.border_color = 48, 66, 44
        self.cell_size = 100

    def refresh(self, server_data: ServerData) -> None:
        """ Обновить данные сетки """
        self.relative_room_left_border = Settings.HALF_WIDTH_WINDOW - server_data.x_pos
        self.relative_room_top_border = Settings.HALF_HEIGHT_WINDOW - server_data.y_pos
        self.relative_room_bottom_border = self.relative_room_top_border + self.height
        self.relative_room_right_border = self.relative_room_left_border + self.width

        self.top = 0
        self.left = 0
        self.right = Settings.WIDTH_WINDOW
        self.bottom = Settings.HEIGHT_WINDOW

        if self.relative_room_bottom_border <= Settings.HEIGHT_WINDOW:
            self.bottom = self.relative_room_bottom_border

        if self.relative_room_right_border <= Settings.WIDTH_WINDOW:
            self.right = self.relative_room_right_border

        if self.relative_room_left_border >= 0:
            self.left = self.relative_room_left_border

        if self.relative_room_top_border >= 0:
            self.top = self.relative_room_top_border

    def draw_lines(self) -> None:
        """ Нарисовать сетку """
        shift_x = 0
        if self.relative_room_left_border < 0:
            shift_x = self.relative_room_left_border

        shift_y = 0
        if self.relative_room_top_border < 0:
            shift_y = self.relative_room_top_border

        for x_coord in range(self.left + shift_x, self.right + 1, self.cell_size):
            pygame.draw.line(
                self.screen, self.border_color,
                (x_coord, self.top), (x_coord, self.bottom),
            )

        for y_coord in range(self.top + shift_y, self.bottom + 1, self.cell_size):
            pygame.draw.line(
                self.screen, self.border_color,
                (self.left, y_coord), (self.right, y_coord),
            )

    def blitme(self) -> None:
        """ Отобразить игровую сетку относительно игрока """
        field = pygame.Rect(self.left, self.top, self.right, self.bottom)
        field_color = (80, 117, 73)
        pygame.draw.rect(self.screen, field_color, field)
        self.draw_lines()
