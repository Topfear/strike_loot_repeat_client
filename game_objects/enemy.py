import pygame
from pygame.surface import Surface

from game_objects.character import Character
from settings import Settings


class Enemy(Character):
    """ Враг """

    def __init__(                                               # noqa
            self, screen: Surface, x_pos: int, y_pos: int,
            radius: int, player_id: int, angle: int) -> None:
        """ Инициализация класса враг """
        super().__init__()
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.player_id = player_id
        self.angle = angle
        self.radius = radius
        self.diameter = radius * 2

        # загрузка картинки
        self.image = pygame.image.load(Settings.IMAGE_MAP[self.player_id])
        self.image = pygame.transform.scale(self.image, (self.diameter, self.diameter))
        self.rect = self.image.get_rect()

        self.rect.centerx = x_pos
        self.rect.centery = y_pos
