import pygame
from pygame.surface import Surface
from pygame.sprite import Sprite


class Bullet(Sprite):
    """ Пуля """

    def __init__(self, screen: Surface, x_pos: int, y_pos: int, radius: int) -> None:
        """ Инициализация класса враг """
        super().__init__()
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.radius = radius
        self.color = (255, 255, 255)
        self.x_pos = x_pos
        self.y_pos = y_pos

    def blitme(self) -> None:
        """ Нарисовать пулю """
        pygame.draw.circle(self.screen, self.color, (self.x_pos, self.y_pos), self.radius)
