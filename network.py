import json
import re
import socket

from settings import Settings

from utils.datatypes import PlayerMessage, ServerData, ServerInitData


class Network:
    """ Класс содержит инструменты для работы с сетью """

    sock: socket.socket

    @classmethod
    def init(cls) -> None:
        """ Инициализация работы с сетью """
        cls.sock = cls.create_sock()

    @classmethod
    def create_sock(cls) -> socket.socket:
        """ Создание сокета для соединения с сервером """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        sock.connect((Settings.SERVER_IP, Settings.SERVER_PORT))
        return sock

    @classmethod
    def player_init_data(cls) -> ServerInitData:
        """ Получение начальных данных с сервера """
        data = cls.sock.recv(128)
        data_decoded = cls.read_server_data(data)
        server_init_data = ServerInitData(
            player_id=data_decoded["player_id"],
            radius=data_decoded["radius"],
            room_width=data_decoded["room_width"],
            room_hight=data_decoded["room_hight"],
        )
        return server_init_data

    @classmethod
    def send_client_data(cls, message: PlayerMessage | None) -> None:
        """ Отправление команды на сервер """
        if message:
            message_json = json.dumps(message)
            message_str = f"<{message_json}>"
            cls.sock.send(message_str.encode())

    @classmethod
    def read_server_data(cls, data_bytes: bytes) -> dict:
        """ Расшифровка и чтение данных с сервера """
        decoded_data = data_bytes.decode()
        match = re.search(r"<(.*?)>", decoded_data)
        if not match:
            return {}
        data = match.group(1)
        data = json.loads(data)
        return data

    @classmethod
    def get_server_data(cls) -> ServerData:
        """ Получаение от сервера нового состояния игрового поля """
        data = cls.sock.recv(2 ** 12)
        server_dict = cls.read_server_data(data)
        server_data = ServerData(
            x_pos=server_dict["x_pos"],
            y_pos=server_dict["y_pos"],
            objects=server_dict["objects"],
        )
        return server_data
