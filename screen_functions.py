import pygame
from pygame.surface import Surface

from game_objects.all_group import AllGroup
from game_objects.bullet import Bullet
from game_objects.enemy import Enemy
from game_objects.grid import Grid
from game_objects.player import Player
from utils.datatypes import ServerData


def update_screen(screen: Surface, all_sprites: AllGroup, grid: Grid) -> None:
    """ Рисуем новое состояние игрового поля """
    screen.fill((63, 84, 59))
    grid.blitme()
    for sprite in all_sprites.sprites():
        sprite.blitme()
    pygame.display.flip()


def init_enemies(
        screen: Surface, player: Player, all_sprites: AllGroup,
        server_data: ServerData) -> None:
    """ Инициализация врагов на экране """
    visible_objects = server_data.objects
    for item in visible_objects:
        if item["type"] == "b":
            x_pos = player.x + item["x"]
            y_pos = player.y + item["y"]
            radius = item["r"]
            bullet = Bullet(screen, x_pos, y_pos, radius)
            all_sprites.add(bullet)

        if item["type"] == "p":
            x_pos = player.x + item["x"]
            y_pos = player.y + item["y"]
            radius = item["r"]
            player_id = item["id"]
            angle = item["angle"]
            enemy = Enemy(screen, x_pos, y_pos, radius, player_id, angle)
            all_sprites.add(enemy)


def draw_screen(screen: Surface, player: Player, server_data: ServerData, grid: Grid) -> None:
    """ Рисование объектов на экране """
    all_sprites = AllGroup()
    all_sprites.add(player)

    grid.refresh(server_data)
    init_enemies(screen, player, all_sprites, server_data)
    update_screen(screen, all_sprites, grid)
