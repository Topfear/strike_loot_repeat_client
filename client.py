import pygame

from game_functions import check_events
from game_objects.grid import Grid
from network import Network
from game_objects.player import Player
from screen_functions import draw_screen
from settings import Settings


class Game:
    """ Клиентская часть игры """

    def __init__(self) -> None:
        """
        Устанавливает соединение с сервером и
        инициализирует начальное состояние игры
        """
        # создание соединения с сервером
        Network.init()

        # создание окна игры
        pygame.init()
        self.screen = pygame.display.set_mode(
            (Settings.WIDTH_WINDOW, Settings.HEIGHT_WINDOW))
        pygame.display.set_caption(Settings.WINDOW_TITLE)

        # создание игровых сущностей
        init_data = Network.player_init_data()
        self.player = Player(self.screen, init_data)

        # таймер
        self.clock = pygame.time.Clock()

        # игровая сетка
        self.grid = Grid(self.screen, init_data)

    def run_game(self) -> None:
        """ Запуск игрового цикла """
        while True:
            # установка FPS
            self.clock.tick(Settings.FPS)

            check_events(self.player)
            self.player.update()
            message = self.player.get_info()
            Network.send_client_data(message)
            server_data = Network.get_server_data()
            draw_screen(self.screen, self.player, server_data, self.grid)


if __name__ == "__main__":
    game = Game()
    game.run_game()
