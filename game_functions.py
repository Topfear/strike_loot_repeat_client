import os

import pygame
from pygame.event import Event
from game_objects.player import Player

from utils.datatypes import Point


def check_keydown_events(event: Event, player: Player) -> None:
    """ Обработка нажатия клавиш """
    if event.key == pygame.K_d:
        player.moving_right = True
    elif event.key == pygame.K_a:
        player.moving_left = True
    elif event.key == pygame.K_w:
        player.moving_up = True
    elif event.key == pygame.K_s:
        player.moving_down = True
    elif event.key == pygame.K_ESCAPE:
        os._exit(0)


def check_keyup_events(event: Event, player: Player) -> None:
    """ Обработка отпускания клавиш """
    if event.key == pygame.K_d:
        player.moving_right = False
    elif event.key == pygame.K_a:
        player.moving_left = False
    if event.key == pygame.K_w:
        player.moving_up = False
    elif event.key == pygame.K_s:
        player.moving_down = False


def check_events(player: Player) -> None:
    """ Обработка команд игрока """
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            os._exit(0)
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, player)
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, player)

        # обработка мыши
        if pygame.mouse.get_focused():
            pos = pygame.mouse.get_pos()
            point = Point(pos[0], pos[1])
            player.rotate(point)
